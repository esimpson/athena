// Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration

#include "../../LArClusterCellDumper/EventReaderAlg.h"
#include "../../LArClusterCellDumper/EventReaderBaseAlg.h"

DECLARE_COMPONENT( EventReaderBaseAlg )
DECLARE_COMPONENT( EventReaderAlg ) 